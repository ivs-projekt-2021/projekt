﻿#!/usr/bin/env python3

"""! @brief Calculator Tokenizer"""
##
# @mainpage IVS Project 2021
#
# @section description_tokenizer Description
# IVS 2021 Project - Tokenizer.
#
# Filip Jaroslav Daubner (xdaubn00@stud.fit.vutbr.cz)
##
# @file tokenizer.py
#
# @brief Tokenizer for mathematical expressions
#
# @section description_doxygen_tokenizer Description
# Finite state machine that generates tokens from string
#
# @section libraries_tokenzer Libraries/Modules
# - srandard Random library (https://docs.python.org/3/library/random.html)
#   - Graphics random numbers.
# - local library.py
#   - Calculator library implementation
#
# @section author_doxygen_tokenizer Author(s)
# - Created by Filip J. Daubner 04/15/2021.
# - Edited by Patrik Šuba.
#
# Only for study purposes
# Fakulta informačních technologií
# Vysoké učení technické v Brně

# Imports
import random
from helper import debug

STATES = ["START", "INTEGER", "FLOAT", "ZERO", "HEX", "OCT", "BIN", "SQRT", "R", "ROOT", "RAND"]

class Tokenizer:
    """ Tokenizer class with get_token function """
    expression = ""
    arguments = {}

    def __init__(self, expression, arguments):
        debug("Tokenizing expression '{}'".format(expression), arguments)
        self.expression = expression.lower().replace(",", ".")
        self.arguments = arguments

    def get_token(self):
        """ Returns a token (numer or operator) """
        debug("Getting a token", self.arguments)
        state = "START"
        tmp = ""
        for char in self.expression:
            if state == "START":
                self.expression = self.expression[1:]
                if "1" <= char <= "9":
                    state = "INTEGER"
                    tmp += char
                elif char == "0":
                    state = "ZERO"
                    tmp += char
                elif char == ".":
                    state = "FLOAT"
                    tmp += char
                elif char == "+":
                    return "+"
                elif char == "-":
                    return "-"
                elif char == "^":
                    return "^"
                elif char == "s":
                    state = "SQRT"
                    tmp += char
                elif char == "/":
                    return "/"
                elif char == "*":
                    return "*"
                elif char == "(":
                    return "("
                elif char == ")":
                    return ")"
                elif char == "r":
                    state = "R"
                    tmp += char
                elif char == "!":
                    return "!"
                elif char in [";", " "]:
                    continue
                else:
                    return "Syntax error"
            elif state == "INTEGER":
                if char.isdigit():
                    tmp += char
                    self.expression = self.expression[1:]
                elif char == ".":
                    state = "FLOAT"
                    tmp += char
                    self.expression = self.expression[1:]
                else:
                    return tmp
            elif state == "ZERO":
                if char == "x":
                    state = "HEX"
                    tmp += char
                    self.expression = self.expression[1:]
                elif char == "o":
                    state = "OCT"
                    tmp += char
                    self.expression = self.expression[1:]
                elif char == "b":
                    state = "BIN"
                    tmp += char
                    self.expression = self.expression[1:]
                elif char == ".":
                    state = "FLOAT"
                    tmp += char
                    self.expression = self.expression[1:]
                elif char == "0":
                    self.expression = self.expression[1:]
                elif "1" <= char <= "9":
                    return "Syntax error"
                else:
                    return tmp
            elif state == "FLOAT":
                if char.isdigit():
                    tmp += char
                    self.expression = self.expression[1:]
                else:
                    return tmp
            elif state == "SQRT":
                if self.expression[0:3] == "qrt":
                    tmp += self.expression[0:3]
                    self.expression = self.expression[3:]
                    return tmp
                return "Syntax error"
            elif state == "R":
                if char == "o":
                    state = "ROOT"
                    tmp += char
                    self.expression = self.expression[1:]
                elif char == "a":
                    state = "RAND"
                    tmp += char
                    self.expression = self.expression[1:]
                else:
                    return "Syntax error"
            elif state == "HEX":
                if char.isdigit():
                    tmp += char
                    self.expression = self.expression[1:]
                elif "a" <= char <= "f":
                    tmp += char
                    self.expression = self.expression[1:]
                elif char == ".":
                    return "Syntax error"
                else:
                    return str(int(tmp, 16))
            elif state == "OCT":
                if "0" <= char <= "7":
                    tmp += char
                    self.expression = self.expression[1:]
                elif char in ["8", "9", "."] or "a" < char < "z":
                    return "Syntax error"
                else:
                    return str(int(tmp, 8))
            elif state == "BIN":
                if char in ["0", "1"]:
                    tmp += char
                    self.expression = self.expression[1:]
                elif "1" <= char <= "9":
                    return "Syntax error"
                elif char == ".":
                    return "Syntax error"
                else:
                    return str(int(tmp, 2))
            elif state == "ROOT":
                if self.expression[0:2] == "ot":
                    tmp += self.expression[0:2]
                    self.expression = self.expression[2:]
                    return tmp
                return "Syntax error"
            elif state == "RAND":
                if self.expression[0:2] == "nd":
                    tmp += self.expression[0:2]
                    self.expression = self.expression[2:]
                    return round(random.random(), 3)
                return "Syntax error"
        if state == "HEX":
            return str(int(tmp, 16))
        if state == "OCT":
            return str(int(tmp, 8))
        if state == "BIN":
            return str(int(tmp, 2))
        return tmp
