#!/usr/bin/env python3

"""! @brief Calculator Library"""
##
# @mainpage IVS Project 2021
#
# @section description_library Description
# IVS 2021 Project - Library implementation.
#
# Filip Jaroslav Daubner (xdaubn00@stud.fit.vutbr.cz)
##
# @file library.py
#
# @brief Calculator library implementation
#
# @section description_doxygen_library Description
# Provides calculate() function which resolves mathematical expression
#
# @section libraries_library Libraries/Modules
# - math standard library (https://docs.python.org/3/library/math.html)
#   - Factorial, rounding.
#
# @section notes_doxygen_library Notes
# - Usage: calculate("1+2") returns 3
#
# @section author_doxygen_library Author(s)
# - Created by Filip J. Daubner 04/15/2021.
#
# Only for study purposes
# Fakulta informačních technologií
# Vysoké učení technické v Brně

# Imports
import math
import decimal
from helper import debug
from tokenizer import Tokenizer

decimal.getcontext().prec = 100

def calculate(expression, arguments):
    """! Resolve mathematical expression

    @param expression Mathematical expression to calculate [str]
    @param arguments Runtime arguments of program

    @return Numerical result [str].
    """

    debug("Calculating expression '{}'".format(expression), arguments)

    return resolve_stack(shunting_yard(expression, arguments), arguments)

def shunting_yard(expression, arguments):
    """! Transform expression from infix to postfix

    @param expression Mathematical expression to postfix [str]
    @param arguments Runtime arguments of program

    @return Postfixed expression [list].
    """

    debug("Postfixing expression '{}'".format(expression), arguments)

    tokenizer = Tokenizer(expression, arguments)
    token = tokenizer.get_token()
    operators = []
    postfix = []

    while token != "":
        if token == "Syntax error":
            return token
        if token in ["+", "-", "*", "/", "!", "^", "(", "sqrt", "root"]:
            operators.append(token)
        elif token == ")": #empty operator stack until ( is found
            if len(operators) == 0:
                return "Syntax error"
            while len(operators) > 0:
                operand = operators.pop()
                if operand == "(":
                    break
                postfix.append(operand)
        else:
            postfix.append(token)
        token = tokenizer.get_token()

    while len(operators) > 0:
        operator = operators.pop()
        if operator == "(":
            return "Syntax error"
        postfix.append(operator)

    debug("Postfixed expression '{}'".format(postfix), arguments)

    return postfix

def resolve_stack(stack, arguments):
    """! Resolve postfixed stack

    @param stack     Mathematical stack [list]
    @param arguments Runtime arguments

    @return Numerical result [str].
    """
    result = []

    debug("Evaluating stack '{}'".format(stack), arguments)

    if stack == "Syntax error":
        return stack

    for token in stack:

        debug("Operating with token '{}'".format(token), arguments)
        debug("Current result stack '{}'".format(result), arguments)

        if token == "Syntax error":
            return token

        if token == "root":
            if len(result) >= 2:
                arg1 = result.pop()
                arg2 = result.pop()
                #xth root of y is just y powered to 1/x:
                result.append(str(decimal.Decimal(arg2)**(decimal.Decimal(arg1))**-1))
            else:
                return "Syntax error"
        elif token == "^":
            if len(result) >= 2:
                arg1 = result.pop()
                arg2 = result.pop()
                result.append(str(decimal.Decimal(arg2)**decimal.Decimal(arg1)))
            else:
                return "Syntax error"
        elif token == "+":
            if len(result) >= 2:
                result.append(str(decimal.Decimal(result.pop())+decimal.Decimal(result.pop())))
            else:
                return "Syntax error"
        elif token == "-":
            if len(result) >= 2:
                arg1 = result.pop()
                arg2 = result.pop()
                result.append(str(decimal.Decimal(arg2)-decimal.Decimal(arg1)))
            elif len(result) == 1:
                result.append(str(-1*decimal.Decimal(result.pop())))
            else:
                return "Syntax error"
        elif token == "*":
            if len(result) < 2:
                return "Syntax error"
            result.append(str(decimal.Decimal(result.pop())*decimal.Decimal(result.pop())))
        elif token == "/":
            if len(result) < 2:
                return "Syntax error"
            arg1 = result.pop()
            arg2 = result.pop()
            if decimal.Decimal(arg1)==0:
                return "Division by zero"
            result.append(str(decimal.Decimal(arg2)/decimal.Decimal(arg1)))
        elif token == "sqrt":
            if len(result) < 1:
                return "Syntax error"
            result.append(str(math.sqrt(decimal.Decimal(result.pop()))))
        elif token == "!":
            if len(result) < 1:
                return "Syntax error"
            arg = decimal.Decimal(result.pop())
            if arg > 500:
                return "Factorial too big"
            if arg < 0:
                return "Negative factorial"
            result.append(str(math.factorial(int(arg))))
        else:
            result.append(decimal.Decimal(token))

    debug("Evaluated stack '{}'".format(result), arguments)

    if len(result) > 1:
        return "Syntax error"

    return "{:.14F}".format(decimal.Decimal(result[0])).rstrip('0').rstrip('.')
