#!/usr/bin/env python3

"""! @brief Calculator Tests"""
##
# @mainpage IVS Project 2021
#
# @section description_test Description
# IVS 2021 Project - Tests (TDD).
#
# Filip Jaroslav Daubner (xdaubn00@stud.fit.vutbr.cz)
##
# @file test.py
#
# @brief Calculator tests
#
# @section description_doxygen_test Description
# Tests library calculate() implementation
#
# @section libraries_test Libraries/Modules
# - pytest standard library (https://docs.python.org/3/library/pytest.html)
#   - Test framework
# - local library.py
#   - Calculator library implementation
# - local helper.py
#   - Error throwing
#
# @section notes_doxygen_test Notes
# - Usage: (env) C:\...\Calculator\src>pytest -q test.py
#
# @section author_doxygen_test Author(s)
# - Created by Filip J. Daubner 04/15/2021.
#
# Only for study purposes
# Fakulta informačních technologií
# Vysoké učení technické v Brně

# Imports
import pytest
from library import calculate
from helper import error

ERROR = "Syntax error"

@pytest.mark.parametrize("mathematical_expression, expected_result", [
    ("0", "0"),
    ("0,1", "0.1"),
    ("0.1", "0.1"),
    ("0,", "0"),
    ("0+", ERROR),
    ("2++2", ERROR),
    ("2+2", "4"),
    ("0-", "-0"),
    ("2--2", "-0"),
    ("2-2", "0"),
    ("1-2", "-1"),
    ("2*3", "6"),
    ("0*", ERROR),
    ("6/3", "2"),
    ("6/", ERROR),
    ("6/0", "Division by zero"),
    ("2^3", "8"),
    ("2^", ERROR),
    ("sqrt(4)", "2"),
    ("sqrt(9)", "3"),
    ("sqrt()", ERROR),
    ("0!", "1"),
    ("1!", "1"),
    ("2!", "2"),
    ("3!", "6"),
    ("!", ERROR),
    ("(-1)!", "Negative factorial"),
    ("root(27;3)", "3"),
    ("root(27;)", ERROR),
    ("root(;3)", ERROR),
    ("rand(1)", ERROR),
    ("2*(3+2)", "10"),
    ("(2*(3+2))", "10"),
    ("(3+2", ERROR),
    ("0b0", "0"),
    ("0b1", "1"),
    ("0b2", ERROR),
    ("0b1+0b1", "2"),
    ("0o0", "0"),
    ("0o6", "6"),
    ("0o8", ERROR),
    ("0o6+0o2", "8"),
    ("0x0", "0"),
    ("0xa", "10"),
    ("0og", ERROR),
    ("0xa+0xb", "21"),
])
def test_calculate(mathematical_expression, expected_result):
    """ General tests for all calculator functions (constants) """
    assert calculate(mathematical_expression, {'debug':True}) == expected_result

def test_calculate_random():
    """ Special tests for random numbers (variable) """
    assert 0 <= float(calculate("rand()", {'debug':True})) < 1
    #test that randomly generated number has 5 or more decimal spaces, can fail sometimes:
    assert len(calculate("rand()", {'debug':True})) >= 5
    #test that two randomly generated numbers are not the same, can fail sometimes:
    assert len(calculate("sqrt(rand()*rand())", {'debug':True})) > 5

if __name__ == "__main__":
    error("Usage: '{}'\n".format("pytest -q test.py"), 1)