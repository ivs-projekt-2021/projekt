﻿#!/usr/bin/env python3

"""! @brief Calculator Helper Functions"""
##
# @mainpage IVS Project 2021
#
# @section description_helper Description
# IVS 2021 Project - Helper functions.
#
# Filip Jaroslav Daubner (xdaubn00@stud.fit.vutbr.cz)
##
# @file helper.py
#
# @brief Helper functions
#
# @section description_doxygen_helper Description
# Helper functions for basic debugging, arguments parsing and exiting
#
# @section libraries_helper Libraries/Modules
# - argparse standard library (https://docs.python.org/3/library/argparse.html)
#   - Argument parsing.
# - sys standard library (https://docs.python.org/3/library/sys.html)
#   - Error handling.
#
# @section author_doxygen_helper Author(s)
# - Created by Filip J. Daubner 04/28/2021.
#
# Only for study purposes
# Fakulta informačních technologií
# Vysoké učení technické v Brně

# Imports
import argparse
import sys

def error(exit_code, error_message=''):
    """! Quits program with specific error.

    @param exit_code     Exit code of error (int).
    @param error_message Message to print to stderr (str).

    """

    sys.stderr.write(error_message+"\n")
    sys.exit(exit_code)

def debug(message, arguments):
    """! Prints given message if debug flag has been set.

    @param message   Mesage to print (str).
    @param arguments Runtime arguments (dictionary).

    """

    if arguments['debug']:
        print(message)

def parse_arguments():
    """! Parse input arguments

    @return Parsed arguments [dictionary].
    """

    #Create a parser
    fileget_arguments = argparse.ArgumentParser(
        description='IVS Project 2021: Calculator' )

    #Define arguments
    fileget_arguments.add_argument(
        "-d", "--debug", action='store_true', help='Print debugging info' )

    #Main parsing
    arguments = vars(fileget_arguments.parse_args())

    debug("Calculator.py started with debugging enabled", arguments)
    debug("arguments: " + str(arguments), arguments)

    return arguments
