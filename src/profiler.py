﻿#!/usr/bin/env python3

"""! @brief Calculator Profiling"""
##
# @mainpage IVS Project 2021
#
# @section description_profiling Description
# IVS 2021 Project - Profiling.
#
# Filip Jaroslav Daubner (xdaubn00@stud.fit.vutbr.cz)
##
# @file profiler.py
#
# @brief Calculator profiling
#
# @section description_doxygen_profile Description
# Pofiling for calculate() function
#
# @section libraries_profile Libraries/Modules
# - profile standard library (https://docs.python.org/3/library/profile.html)
#   - Profiling framework
#
# @section notes_doxygen_profile Notes
# - Usage: (env) C:\Users\user\source\repos\Calculator\src>
#            type .\profiler_input_100.txt | .\profiler.py
#
# @section author_doxygen_profile Author(s)
# - Created by Filip J. Daubner 04/28/2021.
#
# Only for study purposes
# Fakulta informačních technologií
# Vysoké učení technické v Brně

# Imports
import sys
import cProfile
from library import calculate

def get_number():
    """! Read numbers from stdin until whitespace is reached.

    @return number from stdin
    """
    number = ""
    character = sys.stdin.read(1)
    while character not in ['', ' ', '\n', '\t']:
        number += character
        character = sys.stdin.read(1)
    return number

def profiling():
    """! Calculate standard deviation from numbers in stdin in one pass

    """
    n = sum_x = sum_x_squared = 0
    number = get_number()
    while len(number) > 0:
        n = calculate("{}+1".format(n), {'debug': False})
        sum_x = calculate("{}+{}".format(sum_x, number), {'debug': False})
        sum_x_squared = calculate("{}+({}^2)".format(sum_x_squared, number), {'debug': False})
        number = get_number()
    mean = calculate("(1/{})*{}".format(n, sum_x), {'debug': False})
    deviation = calculate("sqrt((1/({}-1))*({}-{}*{}^2))" \
                              .format(n, sum_x_squared, n, mean), {'debug': False})
    print(deviation)
    sys.stdout = sys.stderr #print everyting else (profiling info) to stderr

if __name__ == "__main__":
    cProfile.run("profiling()") # cProfile is ran and print statistics to stderr and result of calculation to stdout
    exit(0)
