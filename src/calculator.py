#!/usr/bin/env python3

"""! @brief Calculator GUI"""
##
# @mainpage IVS Project 2021
#
# @section description_main Description
# IVS 2021 Project - Graphical calculator implementation.
#
# @section notes_main Notes
# - Error codes:
#   - 1 - imported as a library
#
# Filip Jaroslav Daubner (xdaubn00@stud.fit.vutbr.cz)
# Patrik Šuba (xsubap00@stud.fit.vutbr.cz)
##
# @file calculator.py
#
# @brief Graphical calculator implementation
#
# Provides graphical interface for library implementation
#
# @section libraries_main Libraries/Modules
# - tkinter (https://docs.python.org/3/library/tkinter.html)
#   - Graphics rendering.
# - local library.py
#   - Calculator library implementation
# - local helper.py
#   - Program helper functions
#
# @section notes_doxygen_example Notes
# - Usage: calculate("1+2") returns 3
#
# @section author_doxygen_example Author(s)
# - Created by Filip J. Daubner.
# - Edited by Patrik Šuba.
#
# Only for study purposes
# Fakulta informačních technologií
# Vysoké učení technické v Brně

## License
# IVS Projekt 2021
# Copyright (C) Filip J.  Daubner, Patrik Šuba, Marek Prymus
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  You can find it in LICENSE file in root folder
# of this repository.  If not, see <https://www.gnu.org/licenses/>.

# Imports
import tkinter
from library import calculate
from helper import debug, error, parse_arguments

BUTTON_SYSTEMS = ["rand", "pow", "sqrt", "root", "BIN", "OCT", "HEX"]

BUTTON_LABELS = [["1", "2", "3", "!", "CE", "\u21E6"], #backspace
                 ["4", "5", "6", "*", "(", ")"],
                 ["7", "8", "9", "/", "+", "="],
                 ["\u2190", "0", "\u2192", ".", "-", ""]] #l_arrow, r_arrow
BUTTON_LAST = ["A","B","C","D","E","F"]


class Application(tkinter.LabelFrame):
    """ Graphical definition class for rendering main app window """
    expression = ""
    index = 0

    def delete_text(self, event):
        """Clear all text from input field"""
        self.expression = ""
        self.setvar(name="equation", value = "")
        self.index = 0

    def delete_letter(self, event, display):
        """Delete last letter from input field"""
        self.expression = self.expression[:self.index - 1] + self.expression[self.index:]
        self.index = self.index - 1
        self.setvar(name="equation", value = self.expression)
        if self.index > len(self.expression):
            self.index = len(self.expression)
        elif self.index < 0:
            self.index = 0
        display.icursor(self.index)

    def insert_text(self, event, string, display):
        """Insert text into input field"""
        self.expression = str(self.expression[:self.index] + string + self.expression[self.index:])
        self.setvar(name="equation", value = self.expression)
        if self.index < len(self.expression):
            self.index = self.index + len(string)
        else:
            self.index = len(self.expression)
        if string == "sqrt()":
            self.index = self.index - 1
        elif string == "root(;)":
            self.index = self.index - 2

        display.icursor(self.index)

    def change_index(self, event, display, shift):
        """Set cursor in input field to specific position"""
        self.index = self.index + shift

        if self.index < 0:
            self.index = 0
        elif self.index > len(self.expression):
            self.index = len(self.expression)
        display.icursor(self.index)

    def mouse_click(self, event, display):
        """Display cursor on right place after clicking"""
        self.index = display.index(tkinter.INSERT)

    def key_pressed(self, event, display, string):
        """Input pressed key into input field"""
        self.expression = self.expression[:self.index] + string + self.expression[self.index:]
        if self.index < len(self.expression):
            self.index = self.index + len(string)
        else:
            self.index = len(self.expression)

    def back_space(self, event, display):
        """Backspace key handler"""
        self.expression = self.expression[:self.index - 1] + self.expression[self.index:]
        self.index = self.index - 1
        if self.index > len(self.expression):
            self.index = len(self.expression)
        elif self.index < 0:
            self.index = 0

    def evaluate(self, event, display):
        """Equals button handler"""
        self.expression = calculate(self.expression, arguments)
        self.setvar(name="equation", value = self.expression)
        self.index = len(self.expression)
        display.icursor(self.index)

    def __init__(self, *args, **kwargs):
        arguments = args[1]
        tkinter.LabelFrame.__init__(self)

        screen = tkinter.Frame(self)
        screen.pack(side="top")

        equation = tkinter.StringVar(self, name="equation")

        display = tkinter.Entry(screen, textvariable=equation, font=("Arial", 12))

        display.bind('<Button-1>', lambda a: self.mouse_click(self, display))
        display.bind('<BackSpace>', lambda b: self.back_space(self, display))
        display.bind('<Return>', lambda c: self.evaluate(self, display))
        display.bind("0", lambda d: self.key_pressed(self, display, "0"))
        display.bind("1", lambda e: self.key_pressed(self, display, "1"))
        display.bind("2", lambda f: self.key_pressed(self, display, "2"))
        display.bind("3", lambda g: self.key_pressed(self, display, "3"))
        display.bind("4", lambda h: self.key_pressed(self, display, "4"))
        display.bind("5", lambda i: self.key_pressed(self, display, "5"))
        display.bind("6", lambda j: self.key_pressed(self, display, "6"))
        display.bind("7", lambda k: self.key_pressed(self, display, "7"))
        display.bind("8", lambda l: self.key_pressed(self, display, "8"))
        display.bind("9", lambda m: self.key_pressed(self, display, "9"))
        display.bind(".", lambda n: self.key_pressed(self, display, "."))
        display.bind(",", lambda n: self.key_pressed(self, display, ","))
        display.bind("/", lambda o: self.key_pressed(self, display, "/"))
        display.bind("*", lambda p: self.key_pressed(self, display, "*"))
        display.bind("-", lambda r: self.key_pressed(self, display, "-"))
        display.bind("+", lambda s: self.key_pressed(self, display, "+"))
        display.bind("A", lambda t: self.key_pressed(self, display, "A"))
        display.bind("B", lambda t: self.key_pressed(self, display, "B"))
        display.bind("C", lambda t: self.key_pressed(self, display, "C"))
        display.bind("D", lambda t: self.key_pressed(self, display, "D"))
        display.bind("E", lambda t: self.key_pressed(self, display, "E"))
        display.bind("F", lambda t: self.key_pressed(self, display, "F"))

        display.grid(columnspan=4, ipadx=60, rowspan=2, ipady=15)

        sys_buttons = tkinter.Frame(self, width=0, height=0, padx=0, pady=0)
        sys_buttons.pack(side="top")

        for x in range(7):
            if BUTTON_SYSTEMS[x] == "rand":
                action_button = tkinter.Button(sys_buttons, text=BUTTON_SYSTEMS[x],
                    padx=0, pady=0, height=1, width=4, font='arial 11 bold',
                    command= lambda: self.insert_text(self, "rand()", display))
            elif BUTTON_SYSTEMS[x] == "pow":
                action_button = tkinter.Button(sys_buttons,
                    text=BUTTON_SYSTEMS[x], padx=0, pady=0, height=1, width=4, font='arial 11 bold',
                    command= lambda: self.insert_text(self, "^", display))
            elif BUTTON_SYSTEMS[x] == "sqrt":
                action_button = tkinter.Button(sys_buttons,
                    text=BUTTON_SYSTEMS[x], padx=0, pady=0, height=1, width=4, font='arial 11 bold',
                    command= lambda: self.insert_text(self, "sqrt()", display))
            elif BUTTON_SYSTEMS[x] == "root":
                action_button = tkinter.Button(sys_buttons,
                    text=BUTTON_SYSTEMS[x], padx=0, pady=0, height=1, width=4, font='arial 11 bold',
                    command= lambda: self.insert_text(self, "root(;)", display))
            elif BUTTON_SYSTEMS[x] == "BIN":
                action_button = tkinter.Button(sys_buttons,
                    text=BUTTON_SYSTEMS[x], padx=0, pady=0, height=1, width=4, font='arial 11 bold',
                    command= lambda: self.insert_text(self, "0b", display))
            elif BUTTON_SYSTEMS[x] == "OCT":
                action_button = tkinter.Button(sys_buttons,
                    text=BUTTON_SYSTEMS[x], padx=0, pady=0, height=1, width=4, font='arial 11 bold',
                    command= lambda: self.insert_text(self, "0o", display))
            elif BUTTON_SYSTEMS[x] == "HEX":
                action_button = tkinter.Button(sys_buttons,
                    text=BUTTON_SYSTEMS[x], padx=0, pady=0, height=1, width=4, font='arial 11 bold',
                    command= lambda: self.insert_text(self, "0x", display))
            action_button.pack(side="left")

        buttons = tkinter.Frame(self, width=200, height=200)

        for row in range(4):
            for col in range(6):
                if row != 3 or col != 5:
                    if BUTTON_LABELS[row][col] == "0":
                        action_button = tkinter.Button(buttons,
                            text=BUTTON_LABELS[row][col], padx=0, pady=0,
                            height=1, width=2, font='arial 25 bold', bg="#9090b3",
                            command= lambda: self.insert_text(self, "0", display))
                        action_button.grid(row=row + 2, column=col)
                    elif BUTTON_LABELS[row][col] == "1":
                        action_button = tkinter.Button(buttons,
                            text=BUTTON_LABELS[row][col], padx=0, pady=0,
                            height=1, width=2, font='arial 25 bold', bg="#9090b3",
                            command= lambda: self.insert_text(self, "1", display))
                        action_button.grid(row=row + 2, column=col)
                    elif BUTTON_LABELS[row][col] == "2":
                        action_button = tkinter.Button(buttons,
                            text=BUTTON_LABELS[row][col], padx=0, pady=0,
                            height=1, width=2, font='arial 25 bold', bg="#9090b3",
                            command= lambda: self.insert_text(self, "2", display))
                        action_button.grid(row=row + 2, column=col)
                    elif BUTTON_LABELS[row][col] == "3":
                        action_button = tkinter.Button(buttons,
                            text=BUTTON_LABELS[row][col], padx=0, pady=0,
                            height=1, width=2, font='arial 25 bold', bg="#9090b3",
                            command= lambda: self.insert_text(self, "3", display))
                        action_button.grid(row=row + 2, column=col)
                    elif BUTTON_LABELS[row][col] == "4":
                        action_button = tkinter.Button(buttons,
                            text=BUTTON_LABELS[row][col], padx=0, pady=0,
                            height=1, width=2, font='arial 25 bold', bg="#9090b3",
                            command= lambda: self.insert_text(self, "4", display))
                        action_button.grid(row=row + 2, column=col)
                    elif BUTTON_LABELS[row][col] == "5":
                        action_button = tkinter.Button(buttons,
                            text=BUTTON_LABELS[row][col], padx=0, pady=0,
                            height=1, width=2, font='arial 25 bold', bg="#9090b3",
                            command= lambda: self.insert_text(self, "5", display))
                        action_button.grid(row=row + 2, column=col)
                    elif BUTTON_LABELS[row][col] == "6":
                        action_button = tkinter.Button(buttons,
                            text=BUTTON_LABELS[row][col], padx=0, pady=0,
                            height=1, width=2, font='arial 25 bold', bg="#9090b3",
                            command= lambda: self.insert_text(self, "6", display))
                        action_button.grid(row=row + 2, column=col)
                    elif BUTTON_LABELS[row][col] == "7":
                        action_button = tkinter.Button(buttons,
                            text=BUTTON_LABELS[row][col], padx=0, pady=0,
                            height=1, width=2, font='arial 25 bold', bg="#9090b3",
                            command= lambda: self.insert_text(self, "7", display))
                        action_button.grid(row=row + 2, column=col)
                    elif BUTTON_LABELS[row][col] == "8":
                        action_button = tkinter.Button(buttons,
                            text=BUTTON_LABELS[row][col], padx=0, pady=0,
                            height=1, width=2, font='arial 25 bold', bg="#9090b3",
                            command= lambda: self.insert_text(self, "8", display))
                        action_button.grid(row=row + 2, column=col)
                    elif BUTTON_LABELS[row][col] == "9":
                        action_button = tkinter.Button(buttons,
                            text=BUTTON_LABELS[row][col], padx=0, pady=0,
                            height=1, width=2, font='arial 25 bold', bg="#9090b3",
                            command= lambda: self.insert_text(self, "9", display))
                        action_button.grid(row=row + 2, column=col)
                    elif BUTTON_LABELS[row][col] == "!":
                        action_button = tkinter.Button(buttons,
                            text=BUTTON_LABELS[row][col], padx=0, pady=0,
                            height=1, width=2, font='arial 25 bold',
                            command= lambda: self.insert_text(self, "!", display))
                        action_button.grid(row=row + 2, column=col)
                    elif BUTTON_LABELS[row][col] == "CE":
                        action_button = tkinter.Button(buttons,
                            text=BUTTON_LABELS[row][col], padx=0, pady=0,
                            height=1, width=2, font='arial 25 bold',
                            command= lambda: self.delete_text(self))
                        action_button.grid(row=row + 2, column=col)
                    elif BUTTON_LABELS[row][col] == "\u21E6":
                        action_button = tkinter.Button(buttons,
                            text=BUTTON_LABELS[row][col], padx=0, pady=0,
                            height=1, width=2, font='arial 25 bold',
                            command= lambda: self.delete_letter(self, display))
                        action_button.grid(row=row + 2, column=col)
                    elif BUTTON_LABELS[row][col] == "*":
                        action_button = tkinter.Button(buttons,
                            text=BUTTON_LABELS[row][col], padx=0, pady=0,
                            height=1, width=2, font='arial 25 bold',
                            command= lambda: self.insert_text(self, "*", display))
                        action_button.grid(row=row + 2, column=col)
                    elif BUTTON_LABELS[row][col] == "(":
                        action_button = tkinter.Button(buttons,
                            text=BUTTON_LABELS[row][col], padx=0, pady=0,
                            height=1, width=2, font='arial 25 bold',
                            command= lambda: self.insert_text(self, "(", display))
                        action_button.grid(row=row + 2, column=col)
                    elif BUTTON_LABELS[row][col] == ")":
                        action_button = tkinter.Button(buttons,
                            text=BUTTON_LABELS[row][col], padx=0, pady=0,
                            height=1, width=2, font='arial 25 bold',
                            command= lambda: self.insert_text(self, ")", display))
                        action_button.grid(row=row + 2, column=col)
                    elif BUTTON_LABELS[row][col] == "/":
                        action_button = tkinter.Button(buttons,
                            text=BUTTON_LABELS[row][col], padx=0, pady=0,
                            height=1, width=2, font='arial 25 bold',
                            command= lambda: self.insert_text(self, "/", display))
                        action_button.grid(row=row + 2, column=col)
                    elif BUTTON_LABELS[row][col] == "+":
                        action_button = tkinter.Button(buttons,
                            text=BUTTON_LABELS[row][col], padx=0, pady=0,
                            height=1, width=2, font='arial 25 bold',
                            command= lambda: self.insert_text(self, "+", display))
                        action_button.grid(row=row + 2, column=col)
                    elif BUTTON_LABELS[row][col] == "=":
                        action_button = tkinter.Button(buttons,
                            text=BUTTON_LABELS[row][col], padx=0, pady=13,
                            height=2, width=2, font='arial 25 bold', bg="#3a3ab3",
                            command= lambda: self.evaluate(self, display))
                        action_button.grid(row=row + 2, rowspan=2, column=col)
                    elif BUTTON_LABELS[row][col] == "\u2190":
                        action_button = tkinter.Button(buttons,
                            text=BUTTON_LABELS[row][col], padx=0, pady=0,
                            height=1, width=2, font='arial 25 bold',
                            command= lambda: self.change_index(self, display, -1))
                        action_button.grid(row=row + 2, column=col)
                    elif BUTTON_LABELS[row][col] == "\u2192":
                        action_button = tkinter.Button(buttons,
                            text=BUTTON_LABELS[row][col], padx=0, pady=0,
                            height=1, width=2, font='arial 25 bold',
                            command= lambda: self.change_index(self, display, 1))
                        action_button.grid(row=row + 2, column=col)
                    elif BUTTON_LABELS[row][col] == ".":
                        action_button = tkinter.Button(buttons,
                            text=BUTTON_LABELS[row][col], padx=0, pady=0,
                            height=1, width=2, font='arial 25 bold',
                            command= lambda: self.insert_text(self, ".", display))
                        action_button.grid(row=row + 2, column=col)
                    elif BUTTON_LABELS[row][col] == "-":
                        action_button = tkinter.Button(buttons,
                            text=BUTTON_LABELS[row][col], padx=0, pady=0,
                            height=1, width=2, font='arial 25 bold',
                            command= lambda: self.insert_text(self, "-", display))
                        action_button.grid(row=row + 2, column=col)

        for x in range(6):
            if BUTTON_LAST[x] == "A":
                action_button = tkinter.Button(buttons, text=BUTTON_LAST[x],
                    height=1, width=2, font='arial 25 bold',
                    command= lambda: self.insert_text(self, "A", display))
            elif BUTTON_LAST[x] == "B":
                action_button = tkinter.Button(buttons, text=BUTTON_LAST[x],
                    height=1, width=2, font='arial 25 bold',
                    command= lambda: self.insert_text(self, "B", display))
            elif BUTTON_LAST[x] == "C":
                action_button = tkinter.Button(buttons, text=BUTTON_LAST[x],
                    height=1, width=2, font='arial 25 bold',
                    command= lambda: self.insert_text(self, "C", display))
            elif BUTTON_LAST[x] == "D":
                action_button = tkinter.Button(buttons, text=BUTTON_LAST[x],
                    height=1, width=2, font='arial 25 bold',
                    command= lambda: self.insert_text(self, "D", display))
            elif BUTTON_LAST[x] == "E":
                action_button = tkinter.Button(buttons, text=BUTTON_LAST[x],
                    height=1, width=2, font='arial 25 bold',
                    command= lambda: self.insert_text(self, "E", display))
            elif BUTTON_LAST[x] == "F":
                action_button = tkinter.Button(buttons, text=BUTTON_LAST[x],
                    height=1, width=2, font='arial 25 bold',
                    command= lambda: self.insert_text(self, "F", display))
            action_button.grid(row=6, column=x, padx=0, pady=0)

        buttons.pack(side="top")

if __name__ == "__main__":
    arguments = parse_arguments()

    debug("Creating main window", arguments)
    window = tkinter.Tk()

    Application(window, arguments)\
        .pack(side="top", fill="both", expand=True, padx=0, pady=0)

    window.resizable(False, False)
    window.title('Calculator')
    window.tk.call('wm', 'iconphoto', window._w, tkinter.PhotoImage(file="icon.png"))

    window.mainloop()

else:
    error(1, "This program is not implemented to be used as a library.")
