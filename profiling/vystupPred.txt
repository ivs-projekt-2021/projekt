Output before profiling optimization

profiler_input_10.txt 

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
        1    0.000    0.000    0.001    0.001 <string>:1(<module>)
        2    0.000    0.000    0.000    0.000 codecs.py:281(getstate)
        2    0.000    0.000    0.000    0.000 cp1250.py:22(decode)
      641    0.000    0.000    0.000    0.000 helper.py:47(debug)
       32    0.000    0.000    0.001    0.000 library.py:41(calculate)
       32    0.000    0.000    0.001    0.000 library.py:54(shunting_yard)
       32    0.000    0.000    0.001    0.000 library.py:97(resolve_stack)
       11    0.000    0.000    0.000    0.000 profiler.py:39(get_number)
        1    0.000    0.000    0.001    0.001 profiler.py:47(profiling)
       32    0.000    0.000    0.000    0.000 tokenizer.py:44(__init__)
      191    0.000    0.000    0.000    0.000 tokenizer.py:49(get_token)
        2    0.000    0.000    0.000    0.000 {built-in method _codecs.charmap_decode}
        1    0.000    0.000    0.001    0.001 {built-in method builtins.exec}
      203    0.000    0.000    0.000    0.000 {built-in method builtins.len}
        1    0.000    0.000    0.000    0.000 {built-in method builtins.print}
        1    0.000    0.000    0.000    0.000 {built-in method math.sqrt}
      322    0.000    0.000    0.000    0.000 {method 'append' of 'list' objects}
        1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}
      514    0.000    0.000    0.000    0.000 {method 'format' of 'str' objects}
      289    0.000    0.000    0.000    0.000 {method 'isdigit' of 'str' objects}
       32    0.000    0.000    0.000    0.000 {method 'lower' of 'str' objects}
      161    0.000    0.000    0.000    0.000 {method 'pop' of 'list' objects}
       60    0.000    0.000    0.000    0.000 {method 'read' of '_io.TextIOWrapper' objects}
       32    0.000    0.000    0.000    0.000 {method 'replace' of 'str' objects}
       64    0.000    0.000    0.000    0.000 {method 'rstrip' of 'str' objects}

profiler_input_100.txt

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
        1    0.000    0.000    0.010    0.010 <string>:1(<module>)
        2    0.000    0.000    0.000    0.000 codecs.py:281(getstate)
        2    0.000    0.000    0.000    0.000 cp1250.py:22(decode)
     5681    0.000    0.000    0.000    0.000 helper.py:47(debug)
      302    0.000    0.000    0.010    0.000 library.py:41(calculate)
      302    0.001    0.000    0.005    0.000 library.py:54(shunting_yard)
      302    0.002    0.000    0.005    0.000 library.py:97(resolve_stack)
      101    0.000    0.000    0.000    0.000 profiler.py:39(get_number)
        1    0.000    0.000    0.010    0.010 profiler.py:47(profiling)
      302    0.000    0.000    0.000    0.000 tokenizer.py:44(__init__)
     1631    0.002    0.000    0.002    0.000 tokenizer.py:49(get_token)
        2    0.000    0.000    0.000    0.000 {built-in method _codecs.charmap_decode}
        1    0.000    0.000    0.010    0.010 {built-in method builtins.exec}
     1733    0.000    0.000    0.000    0.000 {built-in method builtins.len}
        1    0.000    0.000    0.000    0.000 {built-in method builtins.print}
        1    0.000    0.000    0.000    0.000 {built-in method math.sqrt}
     2752    0.000    0.000    0.000    0.000 {method 'append' of 'list' objects}
        1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}
     4654    0.002    0.000    0.002    0.000 {method 'format' of 'str' objects}
     3030    0.000    0.000    0.000    0.000 {method 'isdigit' of 'str' objects}
      302    0.000    0.000    0.000    0.000 {method 'lower' of 'str' objects}
     1331    0.000    0.000    0.000    0.000 {method 'pop' of 'list' objects}
      593    0.000    0.000    0.000    0.000 {method 'read' of '_io.TextIOWrapper' objects}
      302    0.000    0.000    0.000    0.000 {method 'replace' of 'str' objects}
      604    0.000    0.000    0.000    0.000 {method 'rstrip' of 'str' objects}

profiler_input_1000.txt 

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
        1    0.000    0.000    0.099    0.099 <string>:1(<module>)
        2    0.000    0.000    0.000    0.000 codecs.py:281(getstate)
        2    0.000    0.000    0.000    0.000 cp1250.py:22(decode)
    56081    0.005    0.000    0.005    0.000 helper.py:47(debug)
     3002    0.003    0.000    0.094    0.000 library.py:41(calculate)
     3002    0.012    0.000    0.045    0.000 library.py:54(shunting_yard)
     3002    0.023    0.000    0.045    0.000 library.py:97(resolve_stack)
     1001    0.002    0.000    0.002    0.000 profiler.py:39(get_number)
        1    0.002    0.002    0.099    0.099 profiler.py:47(profiling)
     3002    0.002    0.000    0.004    0.000 tokenizer.py:44(__init__)
    16031    0.019    0.000    0.023    0.000 tokenizer.py:49(get_token)
        2    0.000    0.000    0.000    0.000 {built-in method _codecs.charmap_decode}
        1    0.000    0.000    0.099    0.099 {built-in method builtins.exec}
    17033    0.001    0.000    0.001    0.000 {built-in method builtins.len}
        1    0.000    0.000    0.000    0.000 {built-in method builtins.print}
        1    0.000    0.000    0.000    0.000 {built-in method math.sqrt}
    27052    0.003    0.000    0.003    0.000 {method 'append' of 'list' objects}
        1    0.000    0.000    0.000    0.000 {method 'disable' of '_lsprof.Profiler' objects}
    46054    0.021    0.000    0.021    0.000 {method 'format' of 'str' objects}
    33125    0.002    0.000    0.002    0.000 {method 'isdigit' of 'str' objects}
     3002    0.000    0.000    0.000    0.000 {method 'lower' of 'str' objects}
    13031    0.002    0.000    0.002    0.000 {method 'pop' of 'list' objects}
     5896    0.001    0.000    0.001    0.000 {method 'read' of '_io.TextIOWrapper' objects}
     3002    0.000    0.000    0.000    0.000 {method 'replace' of 'str' objects}
     6004    0.001    0.000    0.001    0.000 {method 'rstrip' of 'str' objects}